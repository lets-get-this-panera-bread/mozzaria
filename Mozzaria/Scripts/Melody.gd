extends AudioStreamPlayer


var BPM = 90
var notes = [note(1,"2C"),note(1,"2G"),note(1,"2F"),note(1,"2A"),note(1,"2G"),note(1,"2E"),note(1,"2D"),note(1,"2C")]

static func note(rhythm, pitch):
	return {rhythm = rhythm, pitch = pitch}

func _ready():
	var sum = 0
	for note in notes:
		sum = sum + note.rhythm
	if sum != 8:
		print("ERROR: notes don't equal a measure!") 
func playMelody():
	for note in notes:
		stream = load("res://Audio/Notes/"+note.pitch+".wav")
		play()
		yield(self, "finished")