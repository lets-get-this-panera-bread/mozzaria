extends AudioStreamPlayer

func _ready():
	pass

func playChord(ID):
	stream = load("res://Audio/Chords/" + ID + "Chord.wav")
	play()
