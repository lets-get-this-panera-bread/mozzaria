extends Node

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

const INGREDIENT_SCENE = preload("res://Scenes/Ingredient.tscn")
onready var chordPlayer = $ChordPlayer
onready var melodyPlayer = $MelodyPlayer

# Called wh"en the node enters the scene tree for the first time.
func _ready():
	melodyPlayer.playMelody()
func _createIngredient(ID):
	var ing = INGREDIENT_SCENE.instance()
	ing.ID = ID
	#ing.sprite = load("res://Art/Ingredients/"+ID+".png")
	add_child(ing)